const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const axios = require('axios');
const qs = require('qs');
const request = require('request');

app.use(bodyParser.urlencoded({
  extended: true
}));

app.use(bodyParser.json());

function handleRedirect(req, res, next) {
  if (req.baseUrl.startsWith('/api/')) {

    req.headers.host = "service.narvii.com";
    req.rawHeaders[req.rawHeaders.indexOf("Host") + 1] = "service.narvii.com";
    delete req.headers.origin;
    if (req.rawHeaders.indexOf("Origin") !== -1) {
      req.rawHeaders.splice(req.rawHeaders.indexOf("Origin"), 2);
    }
    req.headers["user-agent"] = "Dalvik/1.6.0 (Linux; U; Android 4.4.2; SM-G955N Build/SM-G955N-user 4.4.2 NRD90M 38018121 release-keys; com.narvii.amino.master/1.9.22278)";
    req.rawHeaders[req.rawHeaders.indexOf("User-Agent") + 1] = "Dalvik/1.6.0 (Linux; U; Android 4.4.2; SM-G955N Build/SM-G955N-user 4.4.2 NRD90M 38018121 release-keys; com.narvii.amino.master/1.9.22278)";
    req.headers["content-type"] = "application/json; charset=utf-8";
    req.rawHeaders[req.rawHeaders.indexOf("Content-Type") + 1] = "application/json; charset=utf-8";
    delete req.headers.referer;
    if (req.rawHeaders.indexOf("Referer") !== -1) {
      req.rawHeaders.splice(req.rawHeaders.indexOf("Referer"), 2);
    }
    delete req.headers.cookie;
    if (req.rawHeaders.indexOf("Cookie") !== -1) {
      req.rawHeaders.splice(req.rawHeaders.indexOf("Cookie"), 2);
    }

    req.baseUrl = "/api/v1" + req.baseUrl.substr(4);
    req.originalUrl = req.baseUrl;
    req.Url = {
      pathname: req.baseUrl,
      path: req.baseUrl,
      href: req.baseUrl,
      _raw: req.baseUrl
    };
    req.params = {'0': req.baseUrl};

    const params = {
      url: 'https://service.narvii.com' + req.baseUrl + ((req.method === "GET" && req.url) ? req.url.replace('/', "") : ""),
      headers: req.headers,
      method: req.method,
      data: req.body
    };
    if (req.method === "POST") {
      params.data = req.body;
    }

    axios.request(params).then(function(resp) {
      res.json(resp.data);
    }).catch(function() {
      }
    );

  } else if (req.baseUrl.startsWith('/iam')) {
    const params = {
      url: 'https://iam.api.cloud.yandex.net/iam/v1/tokens',
      method: 'POST',
      data: '{"yandexPassportOauthToken": "' + process.env.OAUTH + '"}'
    };
    axios.request(params).then(function(resp) {
      res.json(resp.data);
    }).catch(function() {
      }
    );
  } else if (req.baseUrl.startsWith('/speech')) {
    req.body['folderId'] = process.env.FOLDER_ID;
    const headers = {};
    headers['Content-Type'] = 'application/x-www-form-urlencoded';
    headers['Authorization'] = req.headers['authorization'];
    const params = {
      headers: headers,
    };
    console.log(qs.stringify(req.body));
    request({
      headers: headers,
      uri: 'https://tts.api.cloud.yandex.net/speech/v1/tts:synthesize',
      body: qs.stringify(req.body),
      method: 'POST'
    }).pipe(res);
  } else if (req.baseUrl.startsWith("/main") || req.baseUrl.startsWith("/polyfills") || req.baseUrl.startsWith("/runtime") || req.baseUrl.startsWith("/styles") || req.baseUrl.startsWith("/vendor")) {
    res.sendFile(__dirname + '/dist/AminoC' + req.baseUrl);
  } else if (req.baseUrl.startsWith("/favicon")) {
    res.sendFile(__dirname + '/dist/AminoC/favicon.ico');
  } else if (req.baseUrl.startsWith("/assets/i18n/en")) {
    res.sendFile(__dirname + '/dist/AminoC/assets/i18n/en.json');
  } else if (req.baseUrl.startsWith("/assets/i18n/ru")) {
    res.sendFile(__dirname + '/dist/AminoC/assets/i18n/ru.json');
  } else if (req.baseUrl === "" || req.baseUrl.startsWith("/")) {
    res.sendFile(__dirname + '/dist/AminoC/index.html');
  } else {
    next();
  }

}

app.use('*', handleRedirect);
app.listen(process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 8080, process.env.IP   || process.env.OPENSHIFT_NODEJS_IP || '0.0.0.0');
